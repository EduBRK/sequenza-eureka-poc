package br.org.sequenza.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    @GetMapping("/info")
    @HystrixCommand(fallbackMethod = "infoFallback")
    public String info() throws Exception {

        System.out.println("Running Default...");

        if (1 == 1) throw new Exception();
        return "Eureka-Client is running!";
    }

    public String infoFallback() {
        System.out.println("Running Fallback...");
        return "Eureka-Client is running (Fallback)";
    }

}
