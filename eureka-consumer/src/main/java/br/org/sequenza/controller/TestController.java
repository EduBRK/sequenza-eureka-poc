package br.org.sequenza.controller;

import br.org.sequenza.interfaces.RemoteClientConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class TestController {

    @Autowired
    private RemoteClientConsumer remoteClientConsumer;

    public void consumeInfo() {

        String response = null;

        try {
            response = remoteClientConsumer.info();
        } catch(Exception e) {
            System.out.println(e.getLocalizedMessage());
        }

        System.out.println(response);

    }

}
