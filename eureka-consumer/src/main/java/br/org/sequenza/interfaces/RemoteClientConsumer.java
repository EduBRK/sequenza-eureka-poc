package br.org.sequenza.interfaces;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "eureka-client")
public interface RemoteClientConsumer {

    @GetMapping("info")
    String info();

}
