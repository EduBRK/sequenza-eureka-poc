package br.org.sequenza;

import br.org.sequenza.controller.TestController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableFeignClients
public class EurekaConsumerApp {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(EurekaConsumerApp.class, args);

        TestController testController = applicationContext.getBean(TestController.class);
        for (int i = 0; i < 100; i++) {
            testController.consumeInfo();
        }
    }

    @Bean
    public TestController testController () {
        return new TestController();
    }

}
